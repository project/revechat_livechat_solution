CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Key Features
 * Installation
 * Configuration
 * FAQ
 * Maintainers


INTRODUCTION
--------------------

In the fast paced ambience of live customer support, the efficient usage of 
valuable time is the main key to both happy customers and professional
management. Traditionally interactions between support agents and customers
have been restricted to one-on-one supports over phones or emails. Live chat 
for customer support has now become the alternative that today’s multitasking 
and net savvy generation is looking for. It offers quick response in 
real time and higher customer satisfaction which can generate more 
conversions. In the era of  automated customer assistance systems, 
the chance to chat with a person in real time through your website 
is a distinct feature. Now just like a sales person in the shop, talk
to the visitors of your website in real-time. 
REVE Chat is powerful and intuitive real-time customer engagement 
software. It puts a live person on your website to personally 
guide and help your visitors, while they go through the various 
sections of your digital display. This helps them to get the most 
out of your web presence, while allowing you to understand their 
diverse needs on a one-to-one basis. REVE Chat is easy to install and
use. So place this customizable REVE Chat widget to your business
and start getting sales conversion and higher customer satisfaction.


REQUIREMENTS
---------------
This module requires drupal 9.x

INSTALLATION
----------------------------------------
* Upload `revechat` directory to `/admin/modules` by clicking button `Install new module`.
* Open your Drupal website.
* Go to "Extend > List" and activate `REVE Chat - Live Chat Software` module 
(under `REVE SYSTEMS` category).
* Go to `Configuration > Web Services > REVE Chat` and follow the instructions.

For further information, please visit our website:
[www.revechat.com](https://www.revechat.com "www.revechat.com")

KEY FEATURES
----------------
* **Real-time Visitor Info: ** Agent as well as Admin can see real 
time visitor  information. It enables you to track the website 
visitor’s country, city, appropriate location, IP address, ISP 
(Internet Service Provider), total time spent in your website, 
and referral website


* **Proactive Chat:** Proactive chat enables agents to initiate a 
Chat request from their Chat Window. Moreover Trigger based 
proactive chat automatically sends customized chat alerts for the 
visitors on the website.

* **Chat Monitoring:** Admin can monitor which agents are chatting 
and the detailed chat conversations between your website visitors 
and agents in real-time. Admin can also whisper the agent if they 
give any wrong information. Here you can monitor the chat 
conversation between agents and website visitors and among your 
agents

* **Click to Call:** Click to call is a powerful solution that 
can be implemented on REVE Chat  to offer real-time voice 
assistance to online customers. It allows visitor and agent 
to initiate Voice and Video Call from their Chat window. 


* **Queuing:** With this chat software handle chat requests 
efficiently when all your agents are busy. REVE live chat 
software gives information to website visitors regarding 
their waiting time in the chat queue

* **Facebook Integration:** REVE Chat allows you to engage 
with your potential customers through your company Facebook page. 
Facebook is very effective tool for business. Smart use of social
media increases sales, retention and customer satisfaction. 
REVE Chat Facebook app makes it easier than ever to engage online 
visitors. Facebook integration 
lets customers reach you directly on your Facebook fan page, whether
 or not you are online, 
increasing customer happiness and organic marketing and outreach


### What makes REVE Chat the best choice for live chat?
* A simple and highly customizable Live chat solution
* User friendly interface and Dashboard
* Intuitive Chat Window
* Innovative and unique features
* 24x7 hour live chat support from our trained experts on any weekday 
(visit revechat.com)


### Some Geeky Facts
* Work across major browsers (Internet Explorer 7+, Firefox, 
Google Chrome, Opera, Safari).
* Average uptime is 99.8%.
* Mobile Optimized Dashboard

Should you need any assistance, feel free to chat with our 
customer advocates on 
www.revechat.com or email us at support@revechat.com

FAQ
---------------------
Q: Do I have to install any software on my server to get this 
working? 
A: REVE Chat module need to be installed from your drupal panel. 
It is a cloud 
based live chat software hence it 
would only activate the script when you install the module.

Q: Why should I use REVE Chat?
A: REVE chat is a complete online sales and support tool to engage 
your website visitors. 
With advanced features such as Click to call, Facebook Chat Integration, 
Queuing, 
Chat Monitoring and others, REVE Chat provides full insights to 
communicate with your visitors to become your loyal customers.

Q: Which web browsers work best with this plugin 
A: Though it is designed to work on almost all the browsers, REVE Chat 
works best in the 
following environment: IE 7+ 
or later (PC), Firefox 2 or later (Mac, PC, or Linux), Safari 2 or later 
(Mac), Google Chrome (PC, Mac).


# Maintainers
 --------------------
 * [REVE Chat](http://revechat.com/)
